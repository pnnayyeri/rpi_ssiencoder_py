#import RPi.GPIO as GPIO
import pigpio
import graycode
import time

# GPIO.setmode(GPIO.BCM)

p = pigpio.pi()

PIN_DAT = 17
PIN_CLK = 27
# Tclock = 0.00000025 # 1/4MHz: max CLK signal frequency for SSI ENX 16 EASY Absolute
# Tclock = 0.000025   # 1/0.04MHz: min CLK signal frequency for SSI ENX 16 EASY Absolute
Tclock = 0.0000025  # Any custom value between min and max
# tout = 0.000016     # 16us: min timeout for SSI ENX 16 EASY Absolute
# tout = 0.1          # Any custom tout is ok as long as its > 16us
bitcount = 12

# pin setup done here
try:
    #GPIO.setup(PIN_CLK, GPIO.OUT)
    #GPIO.setup(PIN_DAT, GPIO.IN) #, pull_up_down=GPIO.PUD_DOWN)
    #GPIO.output(PIN_CLK, GPIO.HIGH)
    p.set_mode(PIN_CLK, pigpio.OUTPUT)
    p.set_mode(PIN_DAT, pigpio.INPUT)
    p.set_pull_up_down(PIN_DAT, pigpio.PUD_OFF)
    p.write(PIN_CLK, 1)
except:
    print("ERROR. Unable to setup the configuration requested")

#wait some time to start
time.sleep(0.1)

print("GPIO configuration enabled")

def clockup():
    #GPIO.output(PIN_CLK, GPIO.HIGH)
    p.write(PIN_CLK, 1)
    
def clockdown():
    #GPIO.output(PIN_CLK, GPIO.LOW)
    p.write(PIN_CLK, 0)

def read_pos():
    data = 0
    clockdown()
    prev_t = time.time()
    for bit in range(0, bitcount):
        while True:
            if time.time() - prev_t >= Tclock*(1+0.01):
                prev_t = time.time()
                break
        clockup()
        while True:
            if time.time() - prev_t >= Tclock*(0.5):
                prev_t = time.time()
                break
        data <<= 1
        # last_bit = GPIO.input(PIN_DAT)
        last_bit = p.read(PIN_DAT)
        data |= last_bit
        while True:
            if time.time() - prev_t >= Tclock*(1+0.01):
                prev_t = time.time()
                break
        clockdown()
    while True:
        if time.time() - prev_t >= Tclock*(1):
            prev_t = time.time()
            break
    clockup()
    return data
    
try:
    while True:
        print("{0:012b}".format(graycode.gray_code_to_tc(read_pos())))
        #print(graycode.gray_code_to_tc(read_pos()))
        #print(draw_pulse(read_pos()))
        #time.sleep(0.5)
        #prev_t = time.time()
        while True:
            #print(time.time()-prev_t)
            # if GPIO.input(PIN_DAT) == GPIO.HIGH:
            if p.read(PIN_DAT) == 1:
            #    print("Data back to 1")
            #    print(time.time()-prev_t)
                break
        
finally:
    print("cleaning up GPIO")
    #GPIO.cleanup()
    p.stop()
