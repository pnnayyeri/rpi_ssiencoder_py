import RPi.GPIO as GPIO
from threading import Thread
import graycode
import time

class Encoder(object):
    def __init__(self, DAT_PIN, CLK_PIN, Tclock=0.0000025):
        GPIO.setmode(GPIO.BCM)
        self.DAT_PIN = DAT_PIN
        self.CLK_PIN = CLK_PIN
        self.Tclock = Tclock
        # self.Tclock = 0.00000025 # 1/4MHz: max CLK signal frequency for SSI ENX 16 EASY Absolute
        # self.Tclock = 0.000025   # 1/0.04MHz: min CLK signal frequency for SSI ENX 16 EASY Absolute
        # self.Tclock = 0.0000025  # Any custom value between min and max
        # self.Tout = 0.000016     # 16us: min timeout for SSI ENX 16 EASY Absolute
        self.Tout = 0.1          # Any custom tout is ok as long as its bigger than 16us
        self.Bitcount = 12
        self.data = 0

        # pin setup done here
        try:
            GPIO.setup(self.CLK_PIN, GPIO.OUT)
            GPIO.setup(self.DAT_PIN, GPIO.IN)
            GPIO.output(self.CLK_PIN, GPIO.HIGH)
        except:
            print("ERROR. Unable to setup the configuration requested")

        #wait some time to start
        time.sleep(0.1)

        print("GPIO configuration enabled")

    def clockup(self):
        GPIO.output(self.CLK_PIN, GPIO.HIGH)
        
    def clockdown(self):
        GPIO.output(self.CLK_PIN, GPIO.LOW)

    def _update(self):
        while True:
            if GPIO.input(self.DAT_PIN) == GPIO.HIGH:
                data = 0
                self.clockdown()
                prev_t = time.time()
                for bit in range(0, self.Bitcount):
                    while True:
                        if time.time() - prev_t >= self.Tclock*(1+0.01):
                            prev_t = time.time()
                            break
                    self.clockup()
                    while True:
                        if time.time() - prev_t >= self.Tclock*(0.1):
                            prev_t = time.time()
                            break
                    last_bit = GPIO.input(self.DAT_PIN)
                    data <<= 1
                    data |= last_bit
                    while True:
                        if time.time() - prev_t >= self.Tclock*(1+0.01):
                            prev_t = time.time()
                            break
                    self.clockdown()
                while True:
                    if time.time() - prev_t >= self.Tclock*(1):
                        prev_t = time.time()
                        break
                self.clockup()
                self.data = graycode.gray_code_to_tc(data)
    
    def start(self):
        self.t = Thread(name="Encoder Thread", target=self._update, daemon=True).start()
        return
    
    def read_pos(self):
        # self._update()
        return self.data

if __name__=="__main__":
    encoder = Encoder(17,27)
    try:
        while True:
            print("{0:4d}".format(encoder.read_pos()))
            # prev_t = time.time()
            while True:
                if GPIO.input(encoder.DAT_PIN) == GPIO.HIGH:
                # if (time.time() - prev_t) < encoder.Tout:
                    break
    finally:
        print("cleaning up GPIO")
        GPIO.cleanup()
