import RPi.GPIO as GPIO
import graycode
import time

GPIO.setmode(GPIO.BCM)

PIN_DAT = 17
PIN_CLK = 27
Tclock = 0.00000025 # 1/4MHz: max CLK signal frequency for SSI ENX 16 EASY Absolute
# Tclock = 0.000025   # 1/0.04MHz: min CLK signal frequency for SSI ENX 16 EASY Absolute
# Tclock = 0.0000025  # Any custom value between min and max
# tout = 0.000016     # 16us: min timeout for SSI ENX 16 EASY Absolute
# tout = 0.1          # Any custom tout is ok as long as its > 16us
bitcount = 12

# pin setup done here
try:
    GPIO.setup(PIN_CLK, GPIO.OUT)
    GPIO.setup(PIN_DAT, GPIO.IN) #, pull_up_down=GPIO.PUD_DOWN)
    GPIO.output(PIN_CLK, GPIO.HIGH)
except:
    print("ERROR. Unable to setup the configuration requested")

#wait some time to start
time.sleep(0.1)

print("GPIO configuration enabled")

def clockup():
    GPIO.output(PIN_CLK, GPIO.HIGH)
    
def clockdown():
    GPIO.output(PIN_CLK, GPIO.LOW)

def read_pos():
    data = 0
    clockdown()
    prev_t = time.time()
    # time.sleep(Tclock)
    for bit in range(0,bitcount):
        while True:
            if time.time() - prev_t >= Tclock*(1+0.01):
                prev_t = time.time()
                break
        clockup()
        while True:
            if time.time() - prev_t >= Tclock*(0.1):
                prev_t = time.time()
                break
        # time.sleep(0.0000000025)
        last_bit = GPIO.input(PIN_DAT)
        # print(last_bit)
        # print(time.time() - prev_t)
        data <<= 1
        # last_bit = GPIO.input(PIN_DAT)
        # if last_bit == GPIO.LOW: print("Data zero")
        # data |= not(last_bit)
        data |= last_bit
        # time.sleep(Tclock)
        while True:
            if time.time() - prev_t >= Tclock*(1+0.01):
                prev_t = time.time()
                break
        #prev_t = time.time()
        clockdown()
        #time.sleep(0.000000025)
    clockup()
    return data
    
def draw_pulse(byte_array):
    byte_pulse = ""
    for zero_count in range(0,13-len(str(bin(byte_array)))):
        byte_pulse = "_"
    for bit in str(bin(byte_array)):
        if bit == "0":
            byte_pulse += "_"
        elif bit == "1":
            byte_pulse += "¯"
    return byte_pulse

try:
    while True:
        #print("{0:012b}".format(graycode.gray_code_to_tc(read_pos())))
        print(graycode.gray_code_to_tc(read_pos()))
        #print(draw_pulse(read_pos()))
        #time.sleep(0.5)
        #prev_t = time.time()
        while True:
            #print(time.time()-prev_t)
            if GPIO.input(PIN_DAT) == GPIO.HIGH:
            #    print("Data back to 1")
            #    print(time.time()-prev_t)
                break
        
finally:
    print("cleaning up GPIO")
    GPIO.cleanup()
