<h1>Raspberry Pi / Python library for Absolute Encoders</h1>

<h2>Descriptions</h2>
A python code for reading absolute encoders working under SSI (Synchronous Serial Interface) or RS-422 protocol.
I specifically developed the code for Maxon ENX 16 EASY ABSOLUTE encoder which translates the shaft rotation into 12-bit array.
However, this library can be modified for other absoulte encoders, especially for those support SSI.
Please feel free to modify it for your encoder and send me a push request to include your encoder in the main repo.

<h2>Example</h2>
Clond and cd to the repo:

`git clone`

`RPi_SSIEncoder_py`

cd to the desired encoder package and install it using pip:

`cd [Encoder Directory]`

`pip install [Encoder package]`

Import the library into your code and initialize the encoder object:

`from ENX16EASY import Encoder`

`encoder = Encoder([Data_pin], [Clock_pin], optional:[Tclock])`
